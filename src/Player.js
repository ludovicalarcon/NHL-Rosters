export default class Player {
      constructor(name, jerseyNumber, position, id, pic) {
            this.name = name;
            this.jerseyNumber = jerseyNumber;
            this.position = position;
            this.id = id;
            this.pic = pic;
      }
}