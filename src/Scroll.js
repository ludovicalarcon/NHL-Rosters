import React, { Component } from 'react';

class Scroll extends Component {
      render() {
            return (
                  <div style={{overflowY: 'scroll', border: '2px solid #2c3e50', height: '800px'}}>
                        {this.props.children}
                  </div>      
            );
      }
}

export default Scroll;