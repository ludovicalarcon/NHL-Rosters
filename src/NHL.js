class Player {
      constructor(id, name, jerseyNumber, position, pic) {
            this.name = name;
            this.jerseyNumber = jerseyNumber;
            this.position = position;
            this.id = id;
            this.pic = pic;
      }
}

class Team {
      constructor(id, name, abbreviation, pic) {
            this.id = id;
            this.name = name;
            this.pic = pic;
            this.abbreviation = abbreviation;
            this.players = [];
      }

      addPlayer(id, name, jerseyNumber, position, pic) {
            this.players.push(new Player(id, name, jerseyNumber, position, pic));
      }
}

export default class NHL {
      constructor() {
            this.teams = [];
      }

      addTeam(id, name, abbreviation, pic) {
            this.teams[id] = new Team(id, name, abbreviation, pic);
      }
}

export class NHLTeamTheme {
      constructor(color, fontLetter) {
            this.color = color;
            this.fontLetter = fontLetter;
      }
}