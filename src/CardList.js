import React, { Component } from 'react';
import Card from './Card';

class CardList extends Component {
      render() {
            const cardsArray = this.props.roster.map((card) => {
                  return (<Card
                              key={card.id}
                              id={card.id}
                              name={card.name}
                              number={card.jerseyNumber}
                              pos={card.position}
                              img={card.pic}
                              mainColor={this.props.mainColor}
                  />);
            });
            return (<div>{cardsArray}</div>);
      }
}

export default CardList;