import React, { Component } from 'react';

import CardList from './CardList';

import './Team.css'

class Team extends Component {
      render() {
            return (
                  <div className='tc'>
                        <CardList roster={this.props.nhlTeams} mainColor={this.props.color} />
                  </div>
            );
      }
}

export default Team;