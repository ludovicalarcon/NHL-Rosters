export const SharksRoster = [
      {
            id: 1,
            name: 'Logan Couture',
            number: 39,
            pos: 'center'
      },
      {
            id: 2,
            name: 'Joonas Donskoi',
            number: 27,
            pos: 'Right Wing' 
      },
      {
            id: 3,
            name: 'Dylan Gambrell',
            number: 14,
            pos: 'Center' 
      },
      {
            id: 4,
            name: 'Barclay Goodrow',
            number: 23,
            pos: 'Right Wing' 
      },
      {
            id: 5,
            name: 'Evander Kane',
            number: 9,
            pos: 'Left Wing' 
      },
      {
            id: 6,
            name: 'Melker Karisson',
            number: 68,
            pos: 'Center' 
      },
      {
            id: 7,
            name: 'Kevin Labanc',
            number: 62,
            pos: 'Right Wing' 
      },
      {
            id: 8,
            name: 'Timo Meier',
            number: 28,
            pos: 'Right Wing' 
      },
      {
            id: 9,
            name: 'Joe Pavelski',
            number: 8,
            pos: 'Center' 
      },
      {
            id: 10,
            name: 'Marcus Sorensen',
            number: 20,
            pos: 'Left Wing' 
      },
      {
            id: 11,
            name: 'Joe Thornton',
            number: 19,
            pos: 'Center' 
      },
      {
            id: 12,
            name: 'Justin Braun',
            number: 61,
            pos: 'Right Defensemen' 
      },
      {
            id: 13,
            name: 'Brent Burns',
            number: 88,
            pos: 'Right Defensemen' 
      },
      {
            id: 14,
            name: 'Dylan DeMelo',
            number: 74,
            pos: 'Right Defensemen' 
      },
      {
            id: 15,
            name: 'Brenden Dillon',
            number: 4,
            pos: 'Left Defensemen' 
      },
      {
            id: 16,
            name: 'Tim Heed',
            number: 72,
            pos: 'Right Defensemen' 
      },
      {
            id: 17,
            name: 'Joakim Ryan',
            number: 47,
            pos: 'Left Defensemen' 
      },
      {
            id: 18,
            name: 'Marc-Edouard Vlasic',
            number: 44,
            pos: 'Left Defensemen' 
      },
      {
            id: 19,
            name: 'Antoine Bibeau',
            number: 34,
            pos: 'Goalie' 
      },
      {
            id: 20,
            name: 'Aaron Dell',
            number: 30,
            pos: 'Goalie' 
      },
      {
            id: 21,
            name: 'Martin Jones',
            number: 31,
            pos: 'Goalie' 
      },
      {
            id: 22,
            name: 'Josef Korenar',
            number: 32,
            pos: 'Goalie' 
      }
];