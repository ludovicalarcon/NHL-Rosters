import React, { Component } from 'react';

import './TestComponent.css';

class TestComponent extends Component {
      render() {
            // using className instead of class because class is a javascript reserved keyword
            return (
            <div className='f1 tc'>
                  <h1>Hello World</h1>
                  <p>Welcome {this.props.name}</p>
            </div>
            );
      }
}

export default TestComponent