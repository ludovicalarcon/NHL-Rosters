import React, { Component } from 'react'

import './SearchBox.css'

class SearchBox extends Component {
      render() {
            return (
                  <input
                        className='pa2 search'
                        type='serch'
                        placeholder='Search Player'
                        onChange={this.props.searchChange}      
                  />
            );
      }
}

export default SearchBox;