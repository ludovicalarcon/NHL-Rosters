import React, { Component } from 'react';

import './Card.css'

//<img src={`https://robohash.org/${this.props.id}${this.props.name}?200x200&set=set2`} alt='contact robot'/>

class Card extends Component {
      render() {
            return (
                  <div style={{background: this.props.mainColor}} className='tc dib br3 pa4 grow bw2 shadow-5 card'>
                  <img className='head' src={this.props.img} alt={this.props.name} onError={(e)=>{e.target.src='https://nhl.bamcontent.com/images/headshots/current/168x168/skater.jpg'}}/>
                        <div>
                              <h1 id='jerseyNbr'>{this.props.number}</h1>
                              <h1 id='playerName'>{this.props.name}</h1>
                              <h2 id='playerPosition'>{this.props.pos}</h2>
                        </div>
                  </div>
            );
      }
}

export default Card;