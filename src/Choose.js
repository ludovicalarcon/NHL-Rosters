import React, { Component } from 'react'

import './Choose.css'

class Choose extends Component {
      render() {
            const teamsArray = this.props.teams.map(team => {
                  return (
                        <option className='options' key={team.id} value={team.id}>
                              {team.name}
                        </option>
                  );
            });
            return (
                  <div className='select-style'>
                        <select onChange={this.props.teamChange}>
                              {teamsArray}
                        </select>
                  </div>
            );
      }
}

export default Choose;