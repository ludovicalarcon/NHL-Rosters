import React, { Component } from 'react';

import Team from './Team';
import NHL from './NHL';
import Choose from './Choose';
import {NHLTeamTheme} from './NHL';
import SearchBox from './SearchBox';
import Scroll from './Scroll';

import './App.css'

class App extends Component {
      constructor() {
            super();
            this.nhl = new NHL();
            this.state = {
                  roster: [],
                  searchField: "",
                  teams: [],
                  id: 1
            }
            this.NHLTeamTheme = new Map([
                  ['', new NHLTeamTheme('', '')],
                  ['SJS', new NHLTeamTheme('#006272', 'x')],
                  ['MIN', new NHLTeamTheme('#154734', 'n')],
                  ['NJD', new NHLTeamTheme('#CE1126', 'q')],
                  ['NYI', new NHLTeamTheme('#00539B', 'r')],
                  ['NYR', new NHLTeamTheme('#0038A8', 's')],
                  ['PHI', new NHLTeamTheme('#FA4616', 'u')],
                  ['PIT', new NHLTeamTheme('#FFB81C', 'w')],
                  ['BOS', new NHLTeamTheme('#FFB81C', 'b')],
                  ['BUF', new NHLTeamTheme('#041E42', 'c')],
                  ['MTL', new NHLTeamTheme('#A6192E', 'o')],
                  ['OTT', new NHLTeamTheme('#C69214', 't')],
                  ['TOR', new NHLTeamTheme('#002868', '6')],
                  ['CAR', new NHLTeamTheme('#CC0000', 'e')],
                  ['FLA', new NHLTeamTheme('#B9975B', 'l')],
                  ['TBL', new NHLTeamTheme('#00205B', 'z')],
                  ['WSH', new NHLTeamTheme('#041E42', '8')],
                  ['CHI', new NHLTeamTheme('#C8102E', 'f')],
                  ['DET', new NHLTeamTheme('#C8102E', 'j')],
                  ['NSH', new NHLTeamTheme('#FFB81C', 'p')],
                  ['STL', new NHLTeamTheme('#003087', 'y')],
                  ['CGY', new NHLTeamTheme('#C8102E', 'd')],
                  ['COL', new NHLTeamTheme('#6F263D', 'g')],
                  ['EDM', new NHLTeamTheme('#041E42', 'k')],
                  ['VAN', new NHLTeamTheme('#00205B', '7')],
                  ['ANA', new NHLTeamTheme('#FC4C02', 'a')],
                  ['DAL', new NHLTeamTheme('#006341', 'i')],
                  ['LAK', new NHLTeamTheme('#A2AAAD', 'm')],
                  ['CBJ', new NHLTeamTheme('#041E42', 'h')],
                  ['WPG', new NHLTeamTheme('#041E42', '9')],
                  ['ARI', new NHLTeamTheme('#8C2633', 'v')],
                  ['VGK', new NHLTeamTheme('#B9975B', '0')],
            ]);
      }

      componentDidMount() {
            this.fetchNHLData();            
      }

      async fetchNHLTeamById(id) {
            const response = await fetch(`https://statsapi.web.nhl.com/api/v1/teams/${id}/roster`);
            const team = await response.json();
            return team.roster;
      }

      fecthTeamData = (id) => {
            return Promise.resolve(this.fetchNHLTeamById(id).then(roster => {
                  this.nhl.teams[id].players = [];
                  for (let player of roster) {
                        if (!player.jerseyNumber) {
                        this.nhl.teams[id].addPlayer(player.person.id, player.person.fullName,
                              "--", player.position.name,
                              `https://nhl.bamcontent.com/images/headshots/current/168x168/${player.person.id}.jpg`); 
                        }
                        else {
                        this.nhl.teams[id].addPlayer(player.person.id, player.person.fullName,
                              player.jerseyNumber, player.position.name,
                              `https://nhl.bamcontent.com/images/headshots/current/168x168/${player.person.id}.jpg`); 
                        }
                  }
                  this.setState({roster: this.nhl.teams[id].players});
            }));
      }
      
      async fetchNHLTeams() {
            const response = await fetch('https://statsapi.web.nhl.com/api/v1/teams')
            const data = await response.json();
            return data.teams;
      }

      fetchNHLData = () => {
            return Promise.resolve(this.fetchNHLTeams().then(teams => {
                  for (let team of teams) {
                        console.log(team);
                        this.nhl.addTeam(team.id, team.name, team.abbreviation, "");
                  }
                  this.setState({teams: this.nhl.teams});
                  this.fecthTeamData(1);
            }));
      }

      onSearchChange = (event) => {
            this.setState({searchField: event.target.value});
      }

      onTeamChange = (event) => {
            this.fecthTeamData(event.target.value);
            this.setState({id : event.target.value});
      }

      render() {
            const filterRoster = this.state.roster.filter(player => {
                  return player.name.toLowerCase().includes(this.state.searchField.toLocaleLowerCase());
            });
            const team = this.state.teams[parseInt(this.state.id,10)];
            if (!team) {
                  return (
                        <Team
                              nhlTeams={filterRoster}
                              name=""
                              color=""
                        />
                  );
            }
            const fontLetter = this.NHLTeamTheme.get(team.abbreviation).fontLetter;
            const color = this.NHLTeamTheme.get(team.abbreviation).color;
            return (
                  <div className='tc'>
                        <span className='teamName logo'>{fontLetter}</span><h1 className='teamName'>{team.name.toUpperCase()}</h1>
                        <Choose teams={this.state.teams} teamChange={this.onTeamChange}/>
                        <SearchBox searchChange={this.onSearchChange}/>
                        <Scroll>
                              <Team
                                    nhlTeams={filterRoster}
                                    name={team.name}
                                    color={color}
                              />
                        </Scroll>
                  </div>
            );
      }
}

export default App;